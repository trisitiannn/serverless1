var AWS = require("aws-sdk");

AWS.config.update({
  region: "us-east-1"
});

var docClient = new AWS.DynamoDB.DocumentClient();

var table = "books";
var table2 = "authors";

// need for cross-site POST to work
const corsHeaders = {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': 'content-type',
    'Access-Control-Allow-Methods' :'DELETE'
};
//POST
async function newBook(book) {
    book.isbn = parseInt(book.isbn);
    book.pages = parseInt(book.pages);
    book.aid = parseInt(book.aid); // connect it too the author table 
    let params = {
        TableName: table,
        Item: book
    };
    
    const awsRequest = docClient.put(params);
    return awsRequest.promise()
    .then(data => {
        console.log("Yay!");
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(data) //to see code in browser this console log
        };
        return response;
    },err => {
        console.log("Uh-oh!");
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(err)
        };
        return response;
    });
}


async function newAuthor(author) {
    author.aid = parseInt(author.aid);
    let params = {
        TableName: table2,
        Item: author
    };
    
    const awsRequest = docClient.put(params);
    return awsRequest.promise()
    .then(data => {
        console.log("Yay!");
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(data) //to see code in browser this console log
        };
        return response;
    },err => {
        console.log("Uh-oh!");
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(err)
        };
        return response;
    });
}

//GET
async function getBooks(){
    let tableToget ={
        TableName : table
    }
    const awsRequest = docClient.scan(tableToget);
    return awsRequest.promise()
    .then(data => {
        console.log("working");
        const response = {
            statusCode : 200,
            headers: corsHeaders,
            body : JSON.stringify(data)
        };
        return response;
    })
    
}

async function getName(auth){
    var params = {
        TableName : table2,
        Key: {
            "aid" : auth
        }
    }
    docClient.get(params, (err, data) => {
        if(err){
            console.log("Uh-oh!");
            const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(err)
        };
        return response
        }else {
            const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(data) //to see code in browser this console log
        };
        return response
        }
        
    })
}

async function getAuthors(){
    let tableToget ={
        TableName : table2
    }
    const awsRequest = docClient.scan(tableToget);
    return awsRequest.promise()
    .then(data => {
        console.log("working");
        const response = {
            statusCode : 200,
            headers: corsHeaders,
            body : JSON.stringify(data)
        };
        return response;
    })
    
}

//DELETE
async function deleteBook(bookToDelete){
    let paramsForBook = {
        TableName : table,
        isbn : bookToDelete.isbn
    }
    docClient.delete(paramsForBook, (err, data) => {
        if(err){
            console.log("book does not exist!", JSON.stringify(err, null, 2));
        }else{
            console.log("Item deleted!", JSON.stringify(data, null, 2))
        }
    })
}

exports.handler = async (event) => {
    // sample event properties:
    // event.path: '/default/library/books'
    // event.httpMethod: 'POST'
    // event.body: json encoded object

    console.log( event.path );
    console.log( event.httpMethod );
    let body = JSON.parse(event.body);
    console.log( body + "in lambda" );
    
    if ( event.httpMethod == 'POST' && event.path == '/default/library/books') {
        return newBook(body);
    }else if ( event.httpMethod == 'GET' && event.path == '/default/library/books' ){
        return getBooks(body);
    }else if (event.httpMethod == 'POST' && event.path == '/default/library/authors'){
        return newAuthor(body);
    }else if (event.httpMethod == 'GET' && event.path =='/default/library/authors') {
        return getAuthors(body);
    }else if (event.httpMethod == 'DELETE' && event.path == '/default/library/books' ){
        return deleteBook(body);
    }
    
    const response = {
        statusCode: 200,
        headers: corsHeaders,
        body: JSON.stringify('Unrecognized request'),
    };
    return response;
};