import React, { Component } from 'react';

import axios from 'axios';

const baseURL = "https://xp8atziujg.execute-api.us-east-1.amazonaws.com/default/library";

class Author extends Component {
    constructor(props){
        super(props);
        this.state = {
            authorList : []
        }
    }
    addAuthor(ev){
    const inputAid = ev.target.querySelector('[id="aid"]');
    const aid = inputAid.value.trim();

    const inputFname = ev.target.querySelector('[id="fname"]');
    const fname = inputFname.value.trim();

    const inputLname = ev.target.querySelector('[id="lname"]');
    const lname = inputLname.value.trim();
    
    console.log(aid, fname, lname);
    ev.preventDefault();
    
    let newAuthor = {
        aid,
        fname,
        lname
    }
    axios.post(baseURL + "/authors", newAuthor)
    .then(res =>{
        console.log(res);
    })
    
    ev.preventDefault();
    }
    
    componentDidMount(){
    axios.get(baseURL + "/authors")
    .then(res =>{
      this.setState ({
        authorList : res.data.Items
      })
      console.log(res.data);
    })
  }
  
  
    
    render() {
        return (
    <div>
      <div className="py-5 text-center">
        <h2>Authors</h2>
      </div>

      <div className="row">
        <div className="col-md-12 order-md-1">
          <form onSubmit={this.addAuthor.bind(this)} className="needs-validation" noValidate>
            <div className="row">
              <div className="col-md-2 mb-3">
                <label htmlFor="aid">Author ID</label>
                <input type="number" className="form-control" id="aid" defaultValue="1" required />
                <div className="invalid-feedback">
                    An author needs an id
                </div>
              </div>

              <div className="col-md-5 mb-3">
                <label htmlFor="fname">First Name</label>
                <input type="text" className="form-control" id="fname" defaultValue="" required />
                <div className="invalid-feedback">
                    Everybody has a first name!
                </div>
              </div>

              <div className="col-md-5 mb-3">
                <label htmlFor="lname">Last name</label>
                <input type="text" className="form-control" id="lname" defaultValue="" required />
                <div className="invalid-feedback">
                    Everybody has a last name!
                </div>
              </div>

            </div>
            <button className="btn btn-primary btn-lg btn-block" type="submit">Add Author</button>
          </form>
        </div>
      </div>
       <table className="table">
        <thead>
          <tr>
            <th scope="col">Author Id</th>
            <th scope="col">First name</th>
            <th scope="col">Last name</th>
          </tr>
        </thead>
        <tbody>
        <tr>
        
          <td>
          <ul>
          {this.state.authorList.map(author =>
              <li key={author.aid}>
                {author.aid}
              </li>
            )}
          </ul>
          </td>
          <td>
          <ul>
          {this.state.authorList.map(author =>
              <li key={author.fname}>
                {author.fname}
              </li>
            )}
          </ul>
          </td>
          <td>
          <ul>
          {this.state.authorList.map(author =>
              <li key={author.lname}>
                {author.lname}
              </li>
            )}
          </ul>
          </td>
          </tr>
        </tbody>
      </table>
      <footer className="my-5 pt-5 text-muted text-center text-small">
        <p className="mb-1">&copy; 2020 CPSC 2650</p>
      </footer>
    </div>

        )
    }
}


export default Author;
