import React, { Component } from 'react';

import axios from 'axios';

// UPDATE WITH YOUR API ENDPOINT
const baseURL = "https://xp8atziujg.execute-api.us-east-1.amazonaws.com/default/library";

class Books extends Component {
  constructor(props){
    super(props)
    this.state = {
      bookList : [],
      authorList: []
    }
    this.deleteBook.bind(this)
  }
  addBook(ev) {
    const inputISBN = ev.target.querySelector('[id="isbn"]');
    const isbn = inputISBN.value.trim();

    const inputTitle = ev.target.querySelector('[id="title"]');
    const title = inputTitle.value.trim();

    const inputPages = ev.target.querySelector('[id="pages"]');
    const pages = inputPages.value.trim();

    console.log("ISBN: " + isbn);
    console.log("Title: " + title);
    console.log("Pages: " + pages);
    const aid = ev.target.querySelector('[id="authors"]').value

    let newbook = {
      isbn,
      title,
      pages,
      aid
    };

    axios.post(baseURL + "/books", newbook)
      .then(res => {
        console.log(res);
      });

    ev.preventDefault();



    ev.preventDefault();
  }
  //For rendering
  componentDidMount(){
    axios.get(baseURL + "/books")
    .then(res =>{
      this.setState ({
        bookList : res.data.Items
      })
      console.log(this.state.bookList);
    })
    axios.get(baseURL + "/authors")
    .then(res =>{
      this.setState ({
        authorList : res.data.Items
      })
      console.log(this.state.authorList);
    })
  }
  
  
  //To delete
  deleteBook(book){
    axios.delete(baseURL + "/books", book.isbn)
  }
  
  render() {
    //come back to this
    // const noBullet = {
    //   ['list-style'] : none
    // };


    return (
      <div>
      <div className="py-5 text-center">
        <h2>Books</h2>
      </div>

      <div className="row">
        <div className="col-md-12 order-md-1">
          <form onSubmit={this.addBook.bind(this)} className="needs-validation" noValidate>
            <div className="row">
              <div className="col-md-2 mb-3">
                <label htmlFor="isbn">ISBN</label>
                <input type="number" className="form-control" id="isbn" defaultValue="1" required />
                <div className="invalid-feedback">
                    An ISBN is required.
                </div>
              </div>

              <div className="col-md-8 mb-3">
                <label htmlFor="title">Title</label>
                <input type="text" className="form-control" id="title" defaultValue="" required />
                <div className="invalid-feedback">
                    A book title is required.
                </div>
              </div>

              <div className="col-md-2 mb-3">
                <label htmlFor="pages">Pages</label>
                <input type="number" className="form-control" id="pages" defaultValue="100" required />
                <div className="invalid-feedback">
                    The number of pages is required.
                </div>
              </div>
              
              
              <div className="col-md-5 mb-3">
              <label htmlFor="author">Who wrote this? </label>
              <select id="authors" name="authorList">
                {this.state.authorList.map(author =>(
                 <option value={author.aid}>
                  {author.fname} {author.lname}
                  </option> 
                ))}
              </select>
              </div>


                
            </div>
            <button className="btn btn-primary btn-lg btn-block" type="submit">Add book</button>
          </form>
        </div>
      </div>
      
      <table className="table">
        <thead>
          <tr>
            <th scope="col">ISBN</th>
            <th scope="col">Title</th>
            <th scope="col">Pages</th>
          </tr>
        </thead>
        <tbody>
        <tr>
        {this.state.bookList.map(book =>(
    <li key={book._id}><td>{book.isbn}</td><td>{book.title}</td><td>{book.pages}</td></li>  
  ))}

          <td>
          <ul>
          {this.state.bookList.map(book =>
              <li key={book.isbn}>
                {book.isbn}
              </li>
            )}
          </ul>
          </td>
          <td>
          <ul>
          {this.state.bookList.map(book =>
              <li key={book.title}>
                {book.title}
              </li>
            )}
          </ul>
          </td>
          <td>
          <ul>
          {this.state.bookList.map(book =>
              <li key={book.pages}>
                {book.pages}
              </li>
            )}
          </ul>
          </td>

          </tr>
        </tbody>
      </table>

      <footer className="my-5 pt-5 text-muted text-center text-small">
        <p className="mb-1">&copy; 2020 CPSC 2650</p>
      </footer>
    </div>
    );
  }
}

export default Books;
//THIS WORKS BUT FORMATING GETS SUPER WIERD SO I WENT A BIT MORE BRUTE FORCE
// {this.state.bookList.map(book =>(
//     <li key={book._id}><td>{book.isbn}</td><td>{book.title}</td><td>{book.pages}</td></li>  
//   ))}




  